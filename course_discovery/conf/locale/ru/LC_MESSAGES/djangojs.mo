��    "      ,  /   <      �     �          '     9     >     G     T     c     �     �     �     �  3   �  2   �  !        1     9     >  ;   K     �  -   �     �  '   �  )     '   ;     c     i     q     v     z     �  R   �     �    �  ?     %   W  )   }     �     �  #   �  %   �  1   	     5	     >	     E	  >   R	  i   �	  f   �	  X   b
  /   �
     �
  #   �
  l   "  #   �  h   �  ;     P   X  O   �  I   �     C     V     ^     e  +   i  %   �  q   �     -                                                                                    "         !   
                                               	                Unsupported Image extension Add staff member Area of Expertise Blog Facebook Hide changes New Instructor No courses have been created. OFF ON Other Please enter a valid URL. Please specify a type and url for each social link. Please specify a value for each area of expertise. Please upload a instructor image. Preview Save Show changes Social links with the same type must have different titles. Something went wrong! Something went wrong! please try again later. The image dimensions must be  The image file size cannot exceed 1 MB. The image size must be smaller than 256kb There was an error in saving your data. Title Twitter Type URL Update Instructor Update staff member You have successfully created a Studio URL ({studioLinkTag}) for {courseRunDetail} optional Project-Id-Version: edx-platform
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-05-02 13:11+0000
Last-Translator: Валентина Пицик <lushalusha007@gmail.com>
Language-Team: Russian (http://www.transifex.com/open-edx/edx-platform/language/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 Тип изображения не поддерживается Добавить сотрудника Область специализации Блог Facebook Спрятать изменения новый преподаватель Не создано ни одного курса. ВЫКЛ ВКЛ Другое Пожалуйста, укажите корректный URL. Пожалуйста, укажите тип и URL для каждой социальной ссылки. Пожалуйста, укажите значение для каждой области знаний.  Пожалуйста, загрузите изображение инструктора. Предварительный просмотр Сохранить Показать изменения Социальные связи одного типа должны иметь разные названия. Что-то пошло не так! Что-то пошло не так.  Пожалуйста, повторите попытку позже. Размеры изображения должны быть Размер изображения не должен превышать 1 МБ. Размер изображения должен быть меньше 256 КБ При сохранении данных произошла ошибка. Заголовок Twitter Тип URL Обновить преподавателя Изменить сотрудника У Вас есть успешно созданная Студия URL  ({studioLinkTag}) для {courseRunDetail} необязательный 