��    "      ,  /   <      �     �          '     9     >     G     T     c     �     �     �     �  3   �  2   �  !        1     9     >  ;   K     �  -   �     �  '   �  )     '   ;     c     i     q     v     z     �  R   �     �  �  �  #   �     �  
   �     �     �     �       $        A     E     I  %   [  C   �  4   �  3   �     .	  	   7	     A	  D   V	  #   �	  ;   �	  9   �	  2   5
  0   h
  6   �
     �
     �
     �
     �
     �
     	  Z   #     ~                                                                                    "         !   
                                               	                Unsupported Image extension Add staff member Area of Expertise Blog Facebook Hide changes New Instructor No courses have been created. OFF ON Other Please enter a valid URL. Please specify a type and url for each social link. Please specify a value for each area of expertise. Please upload a instructor image. Preview Save Show changes Social links with the same type must have different titles. Something went wrong! Something went wrong! please try again later. The image dimensions must be  The image file size cannot exceed 1 MB. The image size must be smaller than 256kb There was an error in saving your data. Title Twitter Type URL Update Instructor Update staff member You have successfully created a Studio URL ({studioLinkTag}) for {courseRunDetail} optional Project-Id-Version: edx-platform
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-04-18 13:55+0000
Last-Translator: Stefania Trabucchi <stefania.trabucchi@abstract-technology.de>
Language-Team: German (Germany) (http://www.transifex.com/open-edx/edx-platform/language/de_DE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de_DE
Plural-Forms: nplurals=2; plural=(n != 1);
 Nicht unterstützte Bilderweiterung Mitarbeiter hinzufügen Fachgebiet Blog Facebook Änderungen verstecken Neuer Dozent Es sind keine Kurse erstellt worden. AUS EIN Anderer Abschluss Bitte geben Sie eine gültige URL ein Bitte geben Sie für jeden sozialen Link einen Typ und eine URL an. Bitte geben Sie für jedes Fachgebiet einen Wert an. Bitte laden Sie ein Bild des Lehrers/Dozenten hoch. Vorschau Speichern Änderungen anzeigen Soziale Links vom gleichen Typ müssen unterschiedliche Titel haben. Es ist irgendetwas schief gelaufen! Etwas ist schief gelaufen! Bitte später nochmal versuchen. Die Bildabmessungen müssen so gewählt werden, dass sie  Die Dateigröße darf nicht größer sein als 1MB. Die Bildgröße muss kleiner als 256kb betragen. Beim Speichern Ihrer Daten ist ein Fehler aufgetreten. Titel, Namenszusatz Twitter Typ URL Dozent aktualisieren Mitarbeiter aktualisieren Sie haben erfolgreich eine URL ({studioLinkTag}) für den Kurs {courseRunDetail} erstellt. Weitere Angaben 