��          �   %   �      p  B   q     �     �     �     �     �  	   �     �                       	   %  ,   /  	   \  .   f  T   �     �       
                  1  <   7     t     }     �  �  �  G        \  9   m     �     �  9   �  %        8  9   X     �     �     �     �  E   �     6  O   E  �   �  -   �	     �	  #   �	     �	  ;   
     A
  �   N
  0        7     N        
                        	                                                                                           Any frequently asked questions and the answers to those questions. Audit COURSE END DATE COURSE TITLE Close Course End Date Course ID Course Name Course end date Deleted EDIT Edit Full Name Language in which the course is administered Languages No user with the username [{username}] exists. Only staff users are permitted to filter by username. Remove the username parameter. Professional education Staff Start Date Submit This field is required. Title Usernames of users with explicit access to view this catalog Verified optional required Project-Id-Version: edx-platform
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-02-08 08:59+0000
Last-Translator: Nikos Diakos <nikos.diakos.lomnios@gmail.com>
Language-Team: Greek (http://www.transifex.com/open-edx/edx-platform/language/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 Συχνά ερωτήματα και οι απαντήσεις τους Ακροατές ΗΜΕΡΟΜΗΝΙΑ ΛΗΞΗΣ ΤΟΥ ΜΑΘΗΜΑΤΟΣ ΤΙΤΛΟΣ ΜΑΘΗΜΑΤΟΣ Κλείσιμο Ημερομηνία Λήξης του Μαθήματος Ταυτότητα μαθήματος Τίτλος Μαθήματος Ημερομηνία λήξης του μαθήματος Διαγράφηκε ΕΠΕΞΕΡΓΑΣΙΑ Επεξεργασία Ονοματεπώνυμο Γλώσσα στην οποία παρέχεται το μάθημα Γλώσσες Δεν υπάρχει χρήστης με το ψευδώνυμο [{username}]. Tο φιλτράρισμα βάσει ονόματος χρήστη επιτρέπεται μόνο σε χρήστες που ανήκουν στο προσωπικό. Αφαιρέστε την παράμετρο του ονόματος χρήστη. Επαγγελματική κατάρτιση ΠΡΟΣΩΠΙΚΟ Ημερομηνία Έναρξης Υποβολή Το πεδίο αυτό είναι υποχρεωτικό. Τίτλος Τα ονόματα των χρηστών που έχουν αποκλειστικό δικαίωμα εισόδου, για την προβολή αυτού του καταλόγου Με πιστοποίηση ταυτότητας προαιρετικό απαιτείται 